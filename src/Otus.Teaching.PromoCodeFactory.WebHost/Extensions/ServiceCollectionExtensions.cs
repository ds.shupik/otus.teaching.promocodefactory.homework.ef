﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.DataAccess;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Extensions;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddDbContextFactory(
        this IServiceCollection services
    )
    {
        services.AddDbContextFactory<ApplicationDbContext>((serviceProvider, builder) =>
        {
            builder.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            builder.UseSqlite("DataSource=app.db;Foreign Keys=True;", optionsBuilder =>
                {
                    optionsBuilder
                        .MigrationsHistoryTable("__EFMigrationsHistory");
                })
                .UseLoggerFactory(serviceProvider.GetRequiredService<ILoggerFactory>())
                .UseSnakeCaseNamingConvention();
        });
        services.AddTransient<IDbInitializer, DbInitializer>();
        return services;
    }
}