﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromoCodesController
        : ControllerBase
    {
        private readonly EfRepository<Customer> _customerRepository;
        private readonly EfRepository<PromoCode> _promoCodeRepository;
        private readonly EfRepository<Preference> _preferenceRepository;

        public PromoCodesController(
            EfRepository<Customer> customerRepository,
            EfRepository<PromoCode> promoCodeRepository,
            EfRepository<Preference> preferenceRepository
        )
        {
            _customerRepository = customerRepository;
            _promoCodeRepository = promoCodeRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromoCodesAsync()
            =>(await _promoCodeRepository.GetAllAsync()).Select(x => x.Adapt<PromoCodeShortResponse>()).ToList();
        

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var preference =
                (await _preferenceRepository.GetAllAsync(x => x.Name == request.Preference)).FirstOrDefault();
            if (preference == null) return NotFound();
            var customers =
                (await _customerRepository.GetAllAsync(x => x.Preferences.Any(x => x.Id == preference.Id),
                    x => x.Preferences, x => x.PromoCodes)).ToList();

            if (!customers.Any()) return NotFound();
            foreach (var customer in customers)
            {
                await _promoCodeRepository.CreateAsync(new PromoCode
                {
                    Code = request.PromoCode,
                    ServiceInfo = request.ServiceInfo,
                    PreferenceId = preference.Id,
                    PartnerName = request.PartnerName,
                    CustomerId = customer.Id
                });
            }

            return Ok();
        }
    }
}