﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mapster;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly EfRepository<Customer> _customerRepository;
        private readonly EfRepository<CustomerPreference> _preferenceRepository;
        private readonly EfRepository<CustomerPreference> _customerPreferenceRepository;

        public CustomersController(
            EfRepository<Customer> customerRepository,
            EfRepository<CustomerPreference> preferenceRepository,
            EfRepository<CustomerPreference> customerPreferenceRepository
            )
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _customerPreferenceRepository = customerPreferenceRepository;
        }

        [HttpGet]
        public async Task<ActionResult<ICollection<CustomerShortResponse>>> GetCustomersAsync() =>
            (await _customerRepository.GetAllAsync()).Select(x => x.Adapt<CustomerShortResponse>()).ToList();

        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id,x=>x.PromoCodes);
            if (customer == null) return NotFound();
            return customer.Adapt<CustomerResponse>();
        }


        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var customer = request.Adapt<Customer>();
            customer = await _customerRepository.CreateAsync(customer);
            foreach (var preferenceId in request.PreferenceIds)
            {
                var preference = await _preferenceRepository.GetByIdAsync(preferenceId);
                if (preference != null)
                    await _customerPreferenceRepository.CreateAsync(new CustomerPreference()
                    {
                        CustomerId = customer.Id,
                        PreferenceId = preferenceId
                    });
            }
            return Ok();
        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id, x => x.PromoCodes);
            if (customer == null) return NotFound();
            customer = request.Adapt<Customer>();
            await _customerRepository.UpdateAsync(customer);
            return Ok();
        }

        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            await _customerRepository.DeleteAsync(id);
            return Ok();
        }
    }
}