﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Otus.Teaching.PromoCodeFactory.DataAccess;

public class DbInitializer : IDbInitializer
{
    private readonly IDbContextFactory<ApplicationDbContext> _factory;
    private readonly ILogger _logger;
  

    public DbInitializer(
        IDbContextFactory<ApplicationDbContext> factory,
        ILogger<DbInitializer> logger
    )
    {
        _factory = factory;
        _logger = logger;
    }

    public async Task InitializeAsync()
    {
        await using var context = await _factory.CreateDbContextAsync();
        await context.Database.MigrateAsync();

        _logger.LogInformation($"Миграция для {nameof(ApplicationDbContext)} успешно применена");
    }
}