﻿using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess;

public interface IDbInitializer
{
    public Task InitializeAsync();
}