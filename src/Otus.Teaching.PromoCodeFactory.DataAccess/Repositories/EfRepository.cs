﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

public class EfRepository<T> : IRepository<T>
    where T : BaseEntity
{
    private readonly IDbContextFactory<ApplicationDbContext> _factory;

    public EfRepository(IDbContextFactory<ApplicationDbContext> factory)
    {
        _factory = factory;
    }

    public async Task<IEnumerable<T>> GetAllAsync(Expression<Func<T, bool>> filter = null,
        params Expression<Func<T, object>>[] include)
    {
        await using var context = await _factory.CreateDbContextAsync();
        var query = context.Set<T>().AsQueryable();
        if (include != null)
            query = include.Aggregate(query, (current, currentInclude) => current.Include(currentInclude));
        if (filter != null) query = query.Where(filter);
        return await query.ToListAsync();
    }

    public async Task<T> GetByIdAsync(Guid id, params Expression<Func<T, object>>[] include)
    {
        await using var context = await _factory.CreateDbContextAsync();
        var query = context.Set<T>().AsQueryable();
        if (include != null)
            query = include.Aggregate(query, (current, currentInclude) => current.Include(currentInclude));

        return await query.FirstOrDefaultAsync(x => x.Id == id);
    }

    public async Task<T> CreateAsync(T entity)
    {
        await using var context = await _factory.CreateDbContextAsync();
        await context.AddAsync(entity);
        await context.SaveChangesAsync();
        return entity;
    }

    public async Task UpdateAsync(T entity)
    {
        await using var context = await _factory.CreateDbContextAsync();
        context.Update(entity);
        await context.SaveChangesAsync();
    }

    public async Task DeleteAsync(Guid id)
    {
        await using var context = await _factory.CreateDbContextAsync();
        var entity = await context.Set<T>().FirstOrDefaultAsync(x => x.Id == id) ??
                     throw new KeyNotFoundException();
        context.RemoveRange(entity);
        await context.SaveChangesAsync();
    }
}