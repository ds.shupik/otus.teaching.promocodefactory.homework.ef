﻿using System;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

public class CustomerPreference : BaseEntity
{
    public Guid PreferenceId { get; set; }
    public Preference Preference { get; set; }

    public Guid CustomerId { get; set; }
    public Customer Customer { get; set; }
}